const MOVIES_BACKEND_URL = "http://localhost:3000/movies";
const FAV_MOVIES_BACKEND_URL = "http://localhost:3000/favourites";
let moviesList = [];
let favMoviesList = [];
let errorMessage;

/*
   This function should call getMoviesFromBackend() 
   and store that in an the global array 
*/

function getMovies() {
    

}

/*
   This function should fetch the movies from backend using Fetch API
   if any error occured, this function should handle using Promise.reject()
*/

function getMoviesFromBackend() {
  
}

/*
   This function should display the given movies list on to the html page in the following element
   <section class="movies-container" id="moviesContainer">

*/

function displayMoviesToHtml(moviesList) {
    
}

